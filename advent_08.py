#!/usr/bin/env python


def count_digits(layer, digit):
    return sum(digit is c for c in layer)


def least_zero_layer(layers):
    least_zeroes = 99999
    least_zeroes_layer = ""

    for layer in layers:
        zeroes = count_digits(layer, '0')
        if zeroes < least_zeroes:
            least_zeroes = zeroes
            least_zeroes_layer = layer
    return least_zeroes_layer


def merge_layers(layers, wide, tall):
    full_image = []
    for t in range(0, tall):
        row = ""
        for w in range(0, wide):
            dominant = '2'
            for l in layers[::-1]:
                if l[t*25+w] is not '2':
                    dominant = l[t*25+w]
            row += dominant
        full_image.append(row)

    return full_image
                

def main():
    pixels = []
    wide = 25
    tall = 6
    widextall = wide * tall
    with open("advent_08_input") as input:
        pixels = input.read().rstrip()

    layered = [pixels[i:i+widextall] for i in range(0, len(pixels), widextall)]

    least_zeroes_layer = least_zero_layer(layered)
    ones = count_digits(least_zeroes_layer, '1')
    twos = count_digits(least_zeroes_layer, '2')
    print("Lest digit layer ({}): {}\nNumber of 1 digits ({}) multiplied by number of 2 digits ({}) is {}".format(count_digits(least_zeroes_layer, '0'), least_zeroes_layer, ones, twos, ones*twos))

    merged = merge_layers(layered, wide, tall)
    for m in merged:
        print(m)


if __name__ == "__main__":
    main()
