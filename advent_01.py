#!/usr/bin/env python

from math import floor
from sys import exit

def calculate_fuel(mass):
    return floor(int(mass)/3)-2

def test_scenarios():
    if calculate_fuel(12) == 2 and calculate_fuel(14) == 2 and calculate_fuel(1969) == 654 and calculate_fuel(100756) == 33583:
        return True
    else:
        return False


def main():
    fuel = 0

    if test_scenarios == False:
        print("Your calculations are wrong!")
        exit(-1)

    with open('advent_input') as input:
        for line in input:
            tempFuel = calculate_fuel(line)
            fuel += tempFuel
            while (True):
                tempFuel = calculate_fuel(tempFuel)
                if (tempFuel <= 0):
                    break
                fuel += tempFuel

    print("Fuel needed: {}".format(fuel))


if __name__ == "__main__":
    main()
