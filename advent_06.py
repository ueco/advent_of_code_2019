#!/usr/bin/env python2


def count_indirect_orbits(planet, orbit_list):
    orbits = 0
    current = planet
    while True:
        if current in orbit_list:
            orbits += 1
            current = orbit_list[current]
        else:
            break
    return orbits


def count_orbits(orbits):
    no = 0

    direct_orbits = 0
    indirect_orbits = 0
    for k, v in orbits.items():
        direct_orbits += 1
        indirect_orbits += count_indirect_orbits(v, orbits)
    return direct_orbits + indirect_orbits


def shortest_path_between_SAN_and_YOU(orbits):
    path = 0
    you_origin = []
    current_orbit = "YOU"
    while True:
        if current_orbit in orbits:
            you_origin.append(orbits[current_orbit])
            current_orbit = orbits[current_orbit]
        else:
            break

    san_origin = []
    current_orbit = "SAN"
    while True:
        if current_orbit in orbits:
            san_origin.append(orbits[current_orbit])
            current_orbit = orbits[current_orbit]
        else:
            break

    unique_orbits = you_origin + list(set(san_origin) - set(you_origin))
    print(len(unique_orbits))


def main():
    orbits = {}
    with open("advent_06_input") as input:
        orbits = dict(reversed(x.rstrip().split(')')) for x in input)

    #print(count_orbits(orbits))
    shortest_path_between_SAN_and_YOU(orbits)


if __name__ == "__main__":
    main()
