#!/usr/bin/env python

from copy import deepcopy
from sys import exit

def program_alarm(commands, noun, verb):
    commands[1] = noun
    commands[2] = verb

    for idx, opcode in enumerate(commands):
        if idx%4 != 0:
            continue
        try:
            if opcode == 1:
                commands[commands[idx+3]] = commands[commands[idx+1]] + commands[commands[idx+2]]
            elif opcode == 2:
                commands[commands[idx+3]] = commands[commands[idx+1]] * commands[commands[idx+2]]
            elif opcode == 99:
                break
        except IndexError as error:
            #print("Index error: {}".format(error))
            pass

    return commands[0]

def test_case():
    test_opcodes = [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]
    print(program_alarm(test_opcodes))

def main():
    test_case()
    commands = []
    with open("advent_input_02") as input:
        commands = input.read().rstrip().split(',')
        commands = map(int, commands)

    for i in range(0, 100):
        for y in range(0, 100):
            ret_value = program_alarm(deepcopy(commands), i, y)
            if ret_value == 19690720:
                print("Noun: {} | Verb: {}".format(i, y))
                print("100 * noun + verb = {}".format(100*i+y))

if __name__ == "__main__":
    main()
