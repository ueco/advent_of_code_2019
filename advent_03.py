#!/usr/bin/env python

from math import sqrt, floor


class Line:
    def __init__(self, p, q):
        self.p = p
        self.q = q


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


STARTING_POINT = Point(1,1)


def distance(p, q):
    return sqrt((p.x - q.x)**2 + (p.y - q.y)**2)


def is_between(line, p):
    return distance(line.p, p) + distance(line.q, p) == distance(line.p, line.q)


def cross(A, B):
    print("Cross check: A(x1={}, y1={} | x2={}, y2={}) and B(x1={}, y1={} | x2={}, y2={})".format(A.p.x, A.p.y, A.q.x, A.q.y, B.p.x, B.p.y, B.q.x, B.q.y))
    if A.p == STARTING_POINT or A.q == STARTING_POINT or B.p == STARTING_POINT or B.q == STARTING_POINT:
        return -1
    try:
        x = ((A.q.x - A.p.x) * (B.q.x * B.p.y - B.q.y * B.p.x) - (B.q.x - B.p.x) * (A.q.x * A.p.y - A.q.y * A.p.x)) / ((A.q.y - A.p.y) * (B.q.x - B.p.x) - (B.q.y - B.p.y) * (A.q.x - A.p.x))
        y = ((B.q.y - B.p.y) * (A.q.y * A.p.y - A.q.y * A.p.y) - (A.q.y - A.p.y) * (B.q.x * B.p.y - B.q.y * B.p.x)) / ((B.q.y - B.p.y) * (A.q.x - A.p.x) - (A.q.y - A.p.y) * (B.q.x - B.p.x))
        if y == 0 or x == 0:
            return -1
        print("good = x={}, y={}".format(x, y))
        return Point(x, y)
    except ZeroDivisionError:
        pass

    return -1


def split_direction(prev_point, direction):
    if direction[0] == 'R':
        return Point(prev_point.x+int(direction[1:]), prev_point.y)
    elif direction[0] == 'L':
        return Point(prev_point.x-int(direction[1:]), prev_point.y)
    elif direction[0] == 'U':
        return Point(prev_point.x, prev_point.y+int(direction[1:]))
    elif direction[0] == 'D':
        return Point(prev_point.x, prev_point.y-int(direction[1:]))


def directions_to_points(wire):
    points = []
    for idx, direction in enumerate(wire):
        if idx == 0:
            points.append(split_direction(STARTING_POINT, direction))
        else:
            points.append(split_direction(points[idx-1], direction))

    return points


def manhattan_distance(input_string1 = None, input_string2 = None):
    wire1 = []
    wire2 = []
    
    if input_string1 == None or input_string2 == None:
        with open("advent_input_03") as input:
            wire1 = input.readline().rstrip().split(',')
            wire2 = input.readline().rstrip().split(',')
    else:
        wire1 = input_string1.rstrip().split(',')
        wire2 = input_string2.rstrip().split(',')

    points1 = directions_to_points(wire1)
    points2 = directions_to_points(wire2)

    segments1 = []
    for idx, p1 in enumerate(points1):
        if idx == 0:
            segments1.append(Line(STARTING_POINT, p1))
        else:
            segments1.append(Line(points1[idx-1], p1))
        print("Wire 1 segment: A(x={}, y={}) B(x={}, y={})".format(segments1[idx].p.x, segments1[idx].p.y, segments1[idx].q.x, segments1[idx].q.y))
    segments2 = []
    for idy, p2 in enumerate(points2):
        if idy == 0:
            segments2.append(Line(STARTING_POINT, p2))
        else:
            segments2.append(Line(points2[idy-1], p2))
        print("Wire 2 segment: A(x={}, y={}) B(x={}, y={})".format(segments2[idy].p.x, segments2[idy].p.y, segments2[idy].q.x, segments2[idy].q.y))

    crosspoints = []
    for segment1 in segments1:
        for segment2 in segments2:
            cross_check = cross(segment1, segment2)
            if cross_check != -1:
                crosspoints.append(cross_check)
    
    distance = [99999, STARTING_POINT]
    for crosspoint in crosspoints:
        print("Crosspoint x={} y={}".format(crosspoint.x, crosspoint.y))
        distance_new = abs(crosspoint.x - 1) + abs(crosspoint.y - 1)
        if distance_new < distance[0] and crosspoint != STARTING_POINT:
            distance[0] = distance_new
            distance[1] = crosspoint

    print("Closest distance: {} at point x={} y={}".format(distance[0], distance[1].x, distance[1].y))


def main():
    #manhattan_distance()

    #TESTS
    #first_wire="R8,U5,L5,D3"
    #second_wire="U7,R6,D4,L4"
    #manhattan_distance(first_wire, second_wire)

    first_wire="R75,D30,R83,U83,L12,D49,R71,U7,L72"
    second_wire="U62,R66,U55,R34,D71,R55,D58,R83"
    manhattan_distance(first_wire, second_wire)

if __name__ == "__main__":
    main()
