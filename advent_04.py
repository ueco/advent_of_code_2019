#!/usr/bin/env python

from itertools import groupby


def is_in_range(value, range_list):
    return True if value >= range_list[0] and value <= range_list[1] else False


def are_adjacent_equal_and_pair_only(values):
    splitted = [''.join(sames) for _, sames in groupby(values)]
    for part in splitted:
        if len(part) is 2:
            return True
    return False


def does_values_increase(values):
    for i in range(len(values) - 1):
        if values[i] > values[i+1]:
            return False
    return True


def main():
    pass_range = [356261, 846303]

    how_many = 0

    for val in range(pass_range[0], pass_range[1]):
        valstr = str(val)
        if are_adjacent_equal_and_pair_only(valstr) is True and does_values_increase(valstr) is True:
            print("Possible: {}".format(val))
            how_many += 1
    print("{} of possible passwords!".format(how_many))


if __name__ == "__main__":
    main()
